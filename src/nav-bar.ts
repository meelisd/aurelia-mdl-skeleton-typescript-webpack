import { bindable } from 'aurelia-framework';
import { Router } from 'aurelia-router';

export class NavBar {
  @bindable
  router: Router;

  toggleMenu() {
    if (document == null) return;
    var qs = document.querySelector(".mdl-layout");
    if (qs == null) return;

    var materialLayout = qs["MaterialLayout"]
    materialLayout.toggleDrawer();
    return true;
  }
}
